defmodule Myapp do
  @moduledoc """
  Documentation for Myapp.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Myapp.hello
      :world

  """
  def hello do
    :world
  end

  def call(1, arg1, arg2), do: Myapp.Worker1.call(arg1, arg2)
  def call(2, arg1, arg2), do: Myapp.Worker2.call(arg1, arg2)
  def call(3, arg1, arg2), do: Myapp.Worker3.call(arg1, arg2)
  def call(4, arg1, arg2), do: Myapp.Worker4.call(arg1, arg2)
  def call(5), do: Myapp.Worker5.call()
  def call(6, arg1, arg2), do: Myapp.Worker6.call(arg1, arg2)
  def call(7, arg1), do: Myapp.Worker7.call(arg1)
  def call(8, arg1), do: Myapp.Worker8.call(arg1)
  def call(9, arg1), do: Myapp.Worker9.call(arg1)

  def message(message, worker_id) do
    module = String.to_atom("Elixir.Myapp.Worker#{worker_id}")
    module.call(message)
  end

  def fail() do
    Myapp.Worker10.badcall()
  end
end
