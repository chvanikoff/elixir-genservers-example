defmodule Myapp.Worker10 do
  use GenServer
  require Logger

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    Logger.info "Initializing #{__MODULE__}"
    worker_name = __MODULE__ |> to_string() |> String.split(".") |> List.last()
    state = %{
      worker: worker_name
    }
    {:ok, state}
  end

  def call(message) do
    GenServer.call(__MODULE__, {:message, message})
  end

  def badcall() do
    GenServer.call(__MODULE__, :trigger_noclause)
  end

  def handle_call({:message, message}, _from, state) do
    {:reply, "Message \"#{message}\" received by #{state.worker} with PID #{inspect self()}", state}
  end
end
