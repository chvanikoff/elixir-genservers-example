defmodule Myapp.Worker1 do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    worker_name = __MODULE__ |> to_string() |> String.split(".") |> List.last()
    state = %{
      worker: worker_name
    }
    {:ok, state}
  end

  def call(arg1, arg2) do
    GenServer.call(__MODULE__, {arg1, arg2})
  end

  def handle_call({arg1, arg2}, _from, state) do
    {:reply, arg1 + arg2, state}
  end
end
