defmodule Myapp.Worker5 do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    worker_name = __MODULE__ |> to_string() |> String.split(".") |> List.last()
    state = %{
      worker: worker_name
    }
    {:ok, state}
  end

  def call() do
    GenServer.call(__MODULE__, :random)
  end

  def handle_call(:random, _from, state) do
    {:reply, :rand.uniform(), state}
  end
end
